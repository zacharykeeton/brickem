var brickem = brickem || {}

brickem.Paddle = (function(){

    class Paddle {

        constructor() {
            this.length = brickem.Config.canvasWidth * 0.125
            this.height = brickem.Config.canvasWidth * 0.0125
    
            this.pos = {
                x: 0,
                y: 0
            }
        }
    
        update(newX) {
            this.pos.x = newX - this.length / 2
        }
    
        reset(pos) {
            this.pos    = pos
        }
    }

    return Paddle 
})()