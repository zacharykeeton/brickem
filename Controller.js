var brickem = brickem || {}

brickem.Controller = (function(){

    let _canvas    
    let _paddle    
    let _bricksGrid
    let _brickRows 
    let _hud       
    let _gameboard 
    let _ball      
    let _pointer   


    const _calculateMousePosition = function(evt, canvas) {
    
        const rect      = canvas.getBoundingClientRect()
        const root      = document.documentElement
        const mouseX    = evt.clientX - rect.left - root.scrollLeft
        const mouseY    = evt.clientY - rect.top - root.scrollTop
    
        return {
            x: mouseX,
            y: mouseY
        }
    }

    const _handleBallHitRightWall = function() {
        if (_ball.pos.x >= _gameboard.width - _ball.radius && _ball.speed.x > 0) {
            _ball.hitRightWall()
            _ball.speed.x *= -1
            return true // handled
        }
        else {
            return false // unhandled
        }
    }

    const _handleBallHitLeftWall = function() {
        if (_ball.pos.x <= 0 + _ball.radius && _ball.speed.x < 0) {
            _ball.hitLeftWall()
            _ball.speed.x *= -1
            return true // handled
        }
        else {
            return false // unhandled
        }
    }

    const _handleBallHitTopWall = function() {
        if (_ball.pos.y <= 0 + _ball.radius && _ball.speed.y < 0) {
            _ball.hitTopWall()
            _ball.speed.y *= -1
            return true // handled
        }
        else { 
            return false // unhandled
        }
    }

    const _handleBallHitBottomWall = function() { 
        if (_ball.pos.y >= _gameboard.height - _ball.radius) {

            if (_bricksGrid.numVisible !== 0) {
                _ball.die()
                _resetBall()
                _hud.gameLose()                
            }
            return true // handled
        }
        else { 
            return false // unhandled
        }
    }

    const _handleBallHitPaddle = function() {
        // HANDLE BALL 'HIT PADDLE'
        if (
            _ball.pos.y + _ball.radius >= _paddle.pos.y &&
            _ball.pos.y + _ball.radius <= _paddle.pos.y + _paddle.height &&
            _ball.pos.x >= _paddle.pos.x &&
            _ball.pos.x <= _paddle.pos.x + _paddle.length) {

            const centerOfPaddleX = _paddle.pos.x + _paddle.length / 2
            const distanceFromCenterOfPaddle = _ball.pos.x - centerOfPaddleX
            const newXspeed = distanceFromCenterOfPaddle * 0.35


            _ball.speed.x = newXspeed
            _ball.speed.y *= -1
            _ball.hitPaddle()
            
            

            return true // handled

        }
        else {
            return false // unhandled
        }
    }

    const _handleBallHitBrick = function(previousColumnIndex, previousRowIndex, currentColumnIndex, currentRowIndex) {
        // HANDLE BALL 'HIT BRICK'
        try {
            const touchedBrick = _brickRows[currentRowIndex][currentColumnIndex]
            if (touchedBrick.visible) {

                // HIDE THE BRICK
                touchedBrick.destroy()
                _ball.hitBrick()
                _hud.score++
                _bricksGrid.update()

                // handle completed level
                if (_bricksGrid.numVisible === 0) {
                    _bricksGrid.reset()
                    _resetBall()
                    if(_hud.level >= brickem.Config.maxLevel) {
                        _hud.gameWin()
                        return
                    }
                    _hud.newLevel()
                    return
                }

                // BOUNCE THE BALL

                // flag for 'armpit' case (see below)
                let previousRowBrickVisibleAndPreviouColumnBrickVisible = true

                // bounce the ball
                // note: for this all to look right, the frame rate must be above a certain minimum.
                // handle moving across columns (hitting from side)
                if (previousColumnIndex !== currentColumnIndex) { // hit from the side.

                    // if crossing column and previous brick on same row is 
                    //      1\ /2       // still there, do not bounce back (reverse x)
                    // ----------------
                    //        |
                    // ----------------

                    // if crossing column and previous brick on same row is 
                    //     12\          // NOT still there, bounce back (reverse x)
                    //        ---------
                    //        |
                    //        ---------

                    const bounceX = () => {
                        _ball.speed.x *= -1 // bounce back off column
                        previousRowBrickVisibleAndPreviouColumnBrickVisible = false // no armpit (see below)
                    }

                    try {
                        const adjacentBrickFromPreviousColumnOnSameRow = _brickRows[currentRowIndex][previousColumnIndex]
                        if (!adjacentBrickFromPreviousColumnOnSameRow.visible) {
                            bounceX()
                        }
                    } catch (error) {
                        if (/adjacentBrickFromPreviousColumnOnSameRow is undefined/.test(error.message)) {
                            // ball was previously outside bricks grid. bounce anyway
                            bounceX()
                        } else {
                            throw error
                        }
                    }
                }

                // handle moving across rows (hitting from top/bottom)
                if (previousRowIndex !== currentRowIndex) { // hit from the top/bottom

                    // if crossing row and previous brick on same column is 
                    // still there, do not bounce back (reverse y)
                    //        ---------
                    //     1\ |
                    //        ---------
                    //     2/ |
                    //        ---------

                    // if crossing row and previous brick on same column is 
                    // NOTstill there, do not bounce back (reverse y)
                    //    12\ 
                    //        ---------
                    //        |
                    //        ---------

                    const bounceY = () => {
                        _ball.speed.y *= -1 // bounce back off row
                        previousRowBrickVisibleAndPreviouColumnBrickVisible = false // no armpit (see below)
                    }

                    try {
                        const adjacentBrickFromPreviousRowOnSameColumn = _brickRows[previousRowIndex][currentColumnIndex]
                        if (!adjacentBrickFromPreviousRowOnSameColumn.visible) {
                            bounceY()
                        }
                    } catch (error) {
                        if (/_brickRows\[previousRowIndex\]/.test(error.message)) {
                            // ball was previously outside bricks grid. bounce anyway
                            bounceY()
                        } else {
                            throw error
                        }
                    }
                }

                // handle 'armpit' case
                // without this check, the ball will go 'through the armpit'
                if (previousRowBrickVisibleAndPreviouColumnBrickVisible) {

                    // if crossing row and column and both previous bricks 
                    // still there, bounce back (reverse x and y)
                    // ----------------
                    //        |
                    // ----------------
                    //    12/ |
                    //        ---------

                    _ball.speed.x *= -1 // bounce back off column
                    _ball.speed.y *= -1 // bounce back off row
                }
                
                return true // handled
            }
            else {
                return false // unhandled
            }
        } catch (error) {

            if (/brickRows\[currentRowIndex\] is undefined/.test(error.message) || /touchedBrick is undefined/.test(error.message)) {
                // ball went outside bricks region or hit an edge brick for the first time. i.e. the
                // 'previous' brick doesn't exist . do nothing
                return false // unhandled
            } else {
                throw error
            }
        }
    }

    const _updatePaddle = function(x) {
        _paddle.update(x)
    }

    const _updateBall = function(posx, posy, speedx, speedy) {
        _ball.update(posx, posy, speedx, speedy)
    }

    const _resetBall = function() {
    
        const pos = {
            x: _gameboard.width / 2,
            y: _gameboard.height / 2 + 10
        }

        // randomize ball vector
        // const factor = Math.round(Math.random()) // 0 or 1 to eventually make a 0 or -1 to randomly start ball moving left or right
        // const speed = {
        //     x: Math.floor(Math.random() * 10 + 2) * -factor,
        //     y: Math.floor(Math.random() * 5 + 3)
        // }

        // go straight down at hardcoded speed
        const speed = {
            x: 0,
            y: brickem.Config.canvasHeight * 0.008333333
        }

        _ball.reset(pos, speed)
    }

    const _resetPaddle = function() {
        const pos = {
            x: _gameboard.width / 2 - _paddle.length / 2,
            y: _gameboard.height - (_paddle.height + 10)
        }

        _paddle.reset(pos)
    }

    const _setupNewGame = function() {
        _bricksGrid.reset()
        _resetBall()
        _hud.reset()
        _hud.showingStartScreen = false
    }

    class Controller {
        constructor(canvas, { gameboard, hud, bricksGrid, paddle, ball, pointer }) {
            
            _canvas     = canvas 
            
            _paddle     = paddle
            _bricksGrid = bricksGrid
            _brickRows  = bricksGrid.bricks
            _hud        = hud
            _gameboard  = gameboard
            _ball       = ball
            _pointer    = pointer
    
            _resetBall()
            _resetPaddle()
    
            // update paddle on mousemove
            _canvas.addEventListener('mousemove', (evt) => {
    
                const mousePosition = _calculateMousePosition(evt, canvas)
                _updatePaddle(mousePosition.x)
    
                if (brickem.Config.debugging) { // in global config
                    _pointer.update(mousePosition.x, mousePosition.y)
                    //_updateBall(mousePosition.x, mousePosition.y, 4, -4)
                }
            })
    
            // update paddle on mousemove
            _canvas.addEventListener('mousedown', (evt) => {
                if (_hud.showingStartScreen || _hud.showingWinScreen) {
                    _setupNewGame()
                }
            })
        }
    
        update() {
    
            if(_hud.showingStartScreen || _hud.showingWinScreen) return
         
            // previous indices
            const previousColumnIndex = Math.floor(_ball.pos.x / brickem.Config.brickLength)
            const previousRowIndex = Math.floor(_ball.pos.y / brickem.Config.brickHeight)
    
            _ball.update()
    
            // current indices
            const currentColumnIndex = Math.floor(_ball.pos.x / brickem.Config.brickLength)
            const currentRowIndex = Math.floor(_ball.pos.y / brickem.Config.brickHeight)
    
            // each _handle returns a bool, so it will stop checking once it hits the first true (handled) 
            // conditon, else returns false and does nothing.
            return  _handleBallHitRightWall()
                    ||
                    _handleBallHitLeftWall()
                    ||
                    _handleBallHitTopWall()
                    ||
                    _handleBallHitBottomWall()
                    ||
                    _handleBallHitBrick(previousColumnIndex, previousRowIndex, currentColumnIndex, currentRowIndex)
                    ||
                    _handleBallHitPaddle()
            
        }


    }

    return Controller
})()