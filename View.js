var brickem = brickem || {}

brickem.View = (function(){

    let _context
    let _gameboard 
    let _hud       
    let _bricksGrid
    let _paddle    
    let _ball      
    let _pointer   

    const _jumpSound    = new Audio("assets/sounds/Jump.wav");
    const _dieSound     = new Audio("assets/sounds/Die.wav");
    const _coinSound    = new Audio("assets/sounds/Coin.wav");
    const _kickSound    = new Audio("assets/sounds/Kick.wav");
    const _oneUpSound   = new Audio("assets/sounds/1up.wav");
    const _powerUpSound = new Audio("assets/sounds/Powerup.wav");

    const _playJumpSound = function() {
        _playSoundAndRemove(_jumpSound)
    }

    const _playDieSound = function() {
        _playSoundAndRemove(_dieSound)
    }

    const _playCoinSound = function() {
        _playSoundAndRemove(_coinSound)
    }

    const _playKickSound = function() {
        _playSoundAndRemove(_kickSound)
    }

    const _playOneUpSound = function() {
        _playSoundAndRemove(_oneUpSound)
    }

    const _playPowerUpSound = function() {
        _playSoundAndRemove(_powerUpSound)
    }

    const _playSoundAndRemove = function(sound) {
        const thisSoundInstance = sound.cloneNode()
        thisSoundInstance.play();
        delete thisSoundInstance
    }

    const _renderGameboard = function() {
        _context.fillStyle = "#000";
        _context.fillRect(_gameboard.x, _gameboard.y, _gameboard.width, _gameboard.height);
    }

    const _renderHUD = function() {
        _context.fillStyle = 'white';
        _context.font=`${0.025 * brickem.Config.canvasWidth}px "Palatino Linotype", "Book Antiqua", Palatino, serif`;

        const scoreText = `score: ${_hud.score}`

        _context.fillText(scoreText, _gameboard.width * 0.00625, _gameboard.height * 0.04);

        const levelText = `level: ${_hud.level}`
        const levelTextWidth = _context.measureText(levelText).width
        _context.fillText(levelText, _gameboard.width * 0.5 - levelTextWidth / 2, _gameboard.height * 0.04);

        const highScoreText = `hi: ${_hud.highscore}`
        const highScoreTextWidth = _context.measureText(highScoreText).width
        _context.fillText(highScoreText, _gameboard.width - highScoreTextWidth - 5, _gameboard.height * 0.04);
    }

    const _renderStartScreen = function() {
        _renderBricksGrid()
        _context.fillStyle = 'white';
        const startScreenText = 'click to play'
        const startScreenTextWidth = _context.measureText(startScreenText).width
        _context.fillText(startScreenText, _gameboard.width * 0.5 - startScreenTextWidth / 2, _gameboard.height * 0.66667);
    }

    const _renderBricksGrid = function() {
        for (let brickRow of _bricksGrid.bricks) {
            for (let brick of brickRow) {
                _renderBrick(brick)
            }
        }
    }

    const _renderBrick = function(brick) {

        if (brick.visible) {

            let brickColor

            switch (_hud.level) {
                case 1:
                    brickColor = 'gray'
                    break;
                case 2:
                    brickColor = 'blue'
                    break;
                case 3:
                    brickColor = 'green'
                    break;
            
                default:
                    brickColor = 'gray'
                    break;
            }

            const brickPaddingRight = 1
            const brickPaddingBottom = 1
            _context.fillStyle = brickColor;
            _context.fillRect(brick.pos.x, brick.pos.y, brick.length - brickPaddingRight, brick.height - brickPaddingBottom);
        }
    }

    const _renderPaddle = function() {
        _context.fillStyle = "white";
        _context.fillRect(_paddle.pos.x, _paddle.pos.y, _paddle.length, _paddle.height);
    }

    const _renderBall = function() {
        _context.fillStyle = 'red'
        _context.beginPath()
        _context.arc(_ball.pos.x, _ball.pos.y, _ball.radius, 0, Math.PI * 2)
        _context.fill()
    }

    const _renderPointer = function() {
        if (_pointer.pos.x !== null) {

            let textX, textY

            // on left/right side
            textX = _pointer.pos.x <= _gameboard.width / 2 ? _pointer.pos.x + 20 : _pointer.pos.x - 100

            // in top half/bottom half
            textY = _pointer.pos.y <= _gameboard.height / 2 ? textY = _pointer.pos.y + 20 : textY = _pointer.pos.y - 40

            _context.fillStyle = "white";
            _context.fillText(`x: ${_pointer.pos.x}, y:${_pointer.pos.y}`, textX, textY);
            _context.fillText(`col: ${Math.floor(_pointer.pos.x / brickem.Config.brickHeight)}, row: ${Math.floor(_pointer.pos.y / brickem.Config.brickHeight)}`, textX, textY + 15);
        }
    }

    const _renderWinScreen = function() {
        _context.fillStyle = 'white';
        _context.fillText('YOU WIN!', 400, 400);
    }

    class View {
        
        constructor(canvas, { gameboard, hud, bricksGrid, paddle, ball, pointer } ) {

            _context    = canvas.getContext('2d')
            _gameboard  = gameboard
            _hud        = hud
            _bricksGrid = bricksGrid
            _paddle     = paddle
            _ball       = ball
            _pointer    = pointer
    
            _ball.addEventListener('die', (e) => {
                _playDieSound()
            });
    
            _ball.addEventListener('hitpaddle', (e) => {
                _playJumpSound()
            });

            _ball.addEventListener('hitwall', (e) => {
                _playKickSound()
            });

            _hud.addEventListener('newLevel', (e) => {
                _playPowerUpSound()
            });

            _hud.addEventListener('gamewin', (e) => {
                _playOneUpSound()
            });
    
            for (let brickRow of bricksGrid.bricks) {
                for (let brick of brickRow) {
                    brick.addEventListener('destroy', (e) => {
                        _playCoinSound()
                    });
                }
            }
        }

        render(){

            _context.clearRect( 0, 0, _context.canvas.width, _context.canvas.height);

            _renderGameboard()
            _renderHUD()
    
            if (_hud.showingStartScreen) {
                _renderStartScreen()
                return
            }
            else if(_hud.showingWinScreen){
                _renderWinScreen()
                return
            }
    
            _renderPaddle()
            _renderBall()
            _renderBricksGrid()
            _renderPointer()
        }
    }
    
    return View
})()
