class Brick extends EventTarget {
  
    constructor(color, posX, posY, length, height) {
        super()

        this.visible = true
        this.color = color
        this.pos = {
            x: posX,
            y: posY
        }

        this.length = length
        this.height = height
    }

    reset() {
        this.visible = true
        this.dispatchEvent(new Event('reset'))
    }

    destroy() {
        this.hide()
        this.dispatchEvent(new Event('destroy'))
    }

    hide() {
        this.visible = false
    }
}