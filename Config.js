var brickem = brickem || {}

brickem.Config = (function(){

    const   _canvas             = document.getElementById('brickem')
    
    const _canvasParent           = _canvas.parentElement
    const _canvasParentCSSWidth   = _canvasParent.offsetWidth
    
    
    let   _canvasCSSandLogicalWidth     = _canvasParentCSSWidth
    let   _canvasCSSandLogicalHeight    = _canvasParentCSSWidth * 0.75

    if(_canvasCSSandLogicalHeight > window.innerHeight) {
        _canvasCSSandLogicalHeight    = window.innerHeight
        _canvasCSSandLogicalWidth     = window.innerHeight / 0.75
    }


    class Config {
        static get canvasWidth()    { return _canvasCSSandLogicalWidth }
        static get canvasHeight()   { return _canvasCSSandLogicalHeight }
        static get brickLength()    { return _canvasCSSandLogicalWidth / 15.0 }
        static get brickHeight()    { return _canvasCSSandLogicalHeight / 20.0 }

        static get maxLevel()       { return 3 }
        static get numGutterRows()  { return 3 }
        static get framerate()      { return 30 }
        static get debugging()      { return false }
    }

    return Config
})()