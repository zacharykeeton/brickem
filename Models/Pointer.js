var brickem = brickem || {}

brickem.Pointer = (function(){
    class Pointer {
        constructor() {
            this.pos = {
                x: null,
                y: null
            }
        }
    
        update(newX, newY) {
            this.pos.x = newX
            this.pos.y = newY
        }
    }

    return Pointer
})()