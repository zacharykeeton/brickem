var brickem = brickem || {}

brickem.BricksGrid = (function(){

    class BricksGrid extends EventTarget {
        constructor(gameboardWidth, gameboardHeight, brickLength, brickHeight) {
    
            super()

            this.numGutterRows = brickem.Config.numGutterRows // number of top rows to leave blank as a gutter.
    
            // create brick grid
            const numBrickColumns = Math.floor(gameboardWidth / brickLength)
            const numBrickRows = Math.floor((gameboardHeight / 2) / brickHeight)
    
            const brickRows = new Array(numBrickRows)
    
            for (let rowIndex = 0; rowIndex < brickRows.length; rowIndex++) {
    
                const bricks = new Array(numBrickColumns)
    
                for (let col = 0; col < bricks.length; col++) {
                    const newBrick = new Brick('blue', brickLength * col, brickHeight * rowIndex, brickLength, brickHeight)
                    if (rowIndex < this.numGutterRows) newBrick.hide() // leave a gutter at the top (first 3 rows blank)
    
                    bricks[col] = newBrick // add brick to row
                }
    
                brickRows[rowIndex] = bricks // add row to rows
            }
    
            this.bricks = brickRows
    
            // set numVisible
            this.numVisible = this.getNumVisible() // don't want to do that loop every time .numVisible is accessed.
        }
    
        update() {
            this.numVisible = this.getNumVisible()
        }
    
        reset() {
    
            for (let brickRowIndex = 0; brickRowIndex < this.bricks.length; brickRowIndex++) {
    
                const brickRow = this.bricks[brickRowIndex]
    
                for (let brickIndex = 0; brickIndex < brickRow.length; brickIndex++) {
                    const brick = brickRow[brickIndex]
                    brick.visible = brickRowIndex >= this.numGutterRows
                }
            }

            this.dispatchEvent(new Event('reset'));
        }
    
        getNumVisible() {
    
            // reduce the array of arrays and accumulate the 'visible.true's
            const numBricksVisibleAllRows = this.bricks.reduce((numBricksVisibleAllRowsSoFar, brickRow, currentIndex, array) => {
    
                const numBricksVisibleOnThisRow = brickRow.reduce((numBricksVisibleThisRowSoFar, brick, currentIndex, array) => {
                    const newValue = brick.visible ? ++numBricksVisibleThisRowSoFar : numBricksVisibleThisRowSoFar
                    return newValue
                }, 0)
    
                return numBricksVisibleAllRowsSoFar + numBricksVisibleOnThisRow
            }, 0)
    
            return numBricksVisibleAllRows
        }
    }

    return BricksGrid 
})()