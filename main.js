var brickem = brickem || {}

brickem.run = (function() {

    function createCanvas(width, height) {

        const canvas        = document.getElementById('brickem')
        canvas.style.width  = `${width}px`;
        canvas.style.height = `${height}px`;  
        canvas.width        = width
        canvas.height       = height;
        
        return canvas
    }

    const run = function() {
        const canvas        = createCanvas(brickem.Config.canvasWidth, brickem.Config.canvasHeight)

        // MODELs
        const gameboard     = new brickem.Gameboard(0,0,canvas.width, canvas.height)
        const bricksGrid    = new brickem.BricksGrid(gameboard.width, gameboard.height, brickem.Config.brickLength, brickem.Config.brickHeight)
        const models = {
            gameboard,
            bricksGrid,
            paddle        : new brickem.Paddle(),
            ball          : new brickem.Ball(),
            hud           : new brickem.HUD(),
            pointer       : new brickem.Pointer()    
        }
        
        // VIEW
        const view          = new brickem.View(canvas, models)
        
        // CONTROLLER
        const controller    = new brickem.Controller(canvas, models)
    
        // rendering loop
        setInterval(() => {
            controller.update()
            view.render()        
        }, 1000 / brickem.Config.framerate);
    }

    return run
})()

window.onload = function () {
    brickem.run()
}