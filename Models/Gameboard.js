var brickem = brickem || {}

brickem.Gameboard = (function(){

    class Gameboard {
        constructor(x, y, width, height) {
            this.x = x
            this.y = y
            this.width = width
            this.height = height
        }
    }

    return Gameboard 
})()

