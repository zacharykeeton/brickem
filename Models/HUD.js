var brickem = brickem || {}

brickem.HUD = (function() {

    const _handleNewHighScore = function(currentScore, highScore) {

        if (currentScore > highScore) {
            highScore = currentScore
            localStorage.setItem("bricks_highscore", highScore);
        }

        return highScore
    }

    class HUD extends EventTarget {
        constructor() {
            super()
            this.reset()
        }

        reset() {

            console.log('hs', localStorage.getItem("bricks_highscore"))
            this.showingStartScreen = true
            this.showingWinScreen   = false
            this.score              = 0
            this.highscore          = localStorage.getItem("bricks_highscore") || 0
            this.level              = 1
        }

        gameOver(){
            this.highscore = _handleNewHighScore(this.score, this.highscore)
            this.showingStartScreen = true
        }

        gameWin() {

            this.showingWinScreen = true
            this.highscore = _handleNewHighScore(this.score, this.highscore)

            this.dispatchEvent(new Event('gamewin'));
        }

        gameLose() {
            this.gameOver()
            this.dispatchEvent(new Event('gamelose'));
        }

        newLevel(){
            this.level++
            this.dispatchEvent(new Event('newLevel'));
        }
    }

    return HUD
})()