var brickem = brickem || {}

brickem.Ball = (function(){

    class Ball extends EventTarget{

        constructor() {
    
            super()
            
            this.radius = brickem.Config.canvasWidth * 0.009375
            
            this.pos = {
                x: 0,
                y: 0
            }
            
            this.speed = {
                x: 0,
                y: 0
            }
        }
    
        update(newPosX, newPosY, newSpeedX, newSpeedY, newRadius) {
    
            // handle new position (for debugging/testing)
            // allow for new values to be 0
            if(newPosX !== undefined) this.pos.x = newPosX
            if(newPosY !== undefined) this.pos.y = newPosY
    
            // handle new speed (for debugging/testing)
            // do NOT allow for new values to be 0
            if(newSpeedX) this.speed.x = newSpeedX
            if(newSpeedY) this.speed.y = newSpeedY
    
            // update position
            this.pos.x = this.pos.x + this.speed.x // move 'speed.x' pixels to left or right
            this.pos.y = this.pos.y + this.speed.y // move 'speed.y' pixels to up or down       
    
            // update radius
            this.radius = newRadius === undefined ? this.radius : newRadius
        }
    
        reset(pos, speed) {
            this.pos    = pos
            this.speed  = speed
        }
    
        die() {
            this.dispatchEvent(new Event('die'));
        }
    
        hitLeftWall() {
            this.dispatchEvent(new Event('hitwall'));
            this.dispatchEvent(new Event('hitleftwall'));
        }
    
        hitRightWall() {
            this.dispatchEvent(new Event('hitwall'));
            this.dispatchEvent(new Event('hitrightwall'));
        }
    
        hitTopWall() {
            this.dispatchEvent(new Event('hitwall'));
            this.dispatchEvent(new Event('hittopwall'));
        }
    
        hitBottomWall() {
            this.dispatchEvent(new Event('hitwall'));
            this.dispatchEvent(new Event('hitbottomwall'));
        }
    
        hitBrick() {
            this.dispatchEvent(new Event('hitbrick'));
        }
    
        hitPaddle() {
            this.dispatchEvent(new Event('hitpaddle'));
        }
    }

    return Ball 
})()